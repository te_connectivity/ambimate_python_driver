[![pipeline status](https://gitlab.com/te_connectivity/ambimate_python_driver/badges/master/pipeline.svg)](https://gitlab.com/te_connectivity/ambimate_python_driver/commits/master)
[![coverage report](https://te_connectivity.gitlab.io/ambimate_python_driver/coverage.svg)](https://te_connectivity.gitlab.io/ambimate_python-driver/coverage.html)
[![Documentation](https://img.shields.io/badge/Documentation-ambimate_python_raspberry-blue)](https://te_connectivity.gitlab.io/ambimate_python_driver)  

# Ambimate Python Driver

This is driver for TE Connectivity (TE)’s AmbiMate sensor module MS4.

The driver is used in TE Smart Thermostat project which you can find here: https://gitlab.com/te_connectivity/smart_thermostat
