import unittest
import os
import sys
import inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))  # Get current dir
parentdir = os.path.dirname(currentdir)  # get parent dir
driverdir = parentdir + "/"

sys.path.insert(0, driverdir)  # insert path of parent dir in import settings

from ambimate import Ambimate  # import the driver


class Test_driver(unittest.TestCase):

    def setUp(self):
        self.ambimate = Ambimate()
        self.ambimate.is_unittest = True
        self.ambimate.BoolPrint = False

    def test_setup_sensors(self):
        self.ambimate.setup_sensors()

    def test_ask_measure(self):
        self.ambimate.ask_measure()

    def test_wait_measure(self):
        self.ambimate.wait_measure()

    def test_read(self):
        for address_data in range(0xFF+1):
            value = self.ambimate.read(address_data)
            min_value = 0x00
            max_value = 0xFF
            self.assertGreaterEqual(value, min_value)
            self.assertLessEqual(value, max_value)
            self.assertEqual(str(type(value)) in ["<type 'int'>","<class 'int'>"], True)
        with self.assertRaises(ValueError):
            self.ambimate.read(0x00-1)
        with self.assertRaises(ValueError):
            self.ambimate.read(0xFF+1)

    def test_write(self):
        for address_data in range(0xFF+1):
            for data in range(0xFF+1):
                self.ambimate.write(address_data, data)
            with self.assertRaises(ValueError):
                self.ambimate.write(address_data, -1)
            with self.assertRaises(ValueError):
                self.ambimate.write(address_data, 0xFF+1)
        with self.assertRaises(ValueError):
            for data in range(0xFF+1):
                self.ambimate.write(0x00-1, data)
        with self.assertRaises(ValueError):
            for data in range(0xFF+1):
                self.ambimate.write(0xFF+1, data)
        with self.assertRaises(TypeError):
            for data in range(0xFF+1):
                self.ambimate.write(0x00+0.1, data)
        with self.assertRaises(TypeError):
            for address_data in range(0xFF+1):
                self.ambimate.write(address_data, 0x00+0.1)

    def test_get_firmware_version(self):
        data = self.ambimate.get_firmware_version()
        self.assertEqual(data[0], "Firmware")
        self.assertEqual(data[1], self.ambimate.read(0x80))
        self.assertEqual(data[2], "Version")

    def test_get_firmware_subversion(self):
        data = self.ambimate.get_firmware_subversion()
        self.assertEqual(data[0], "SubFirmware")
        self.assertEqual(data[1], self.ambimate.read(0x81))
        self.assertEqual(data[2], "Subversion")

    def test_get_optional_sensors(self):
        data = self.ambimate.get_optional_sensors()
        self.assertEqual(data[0], "Sensors")
        self.assertEqual(data[1], self.ambimate.read(0x82))
        self.assertEqual(data[2], "Option")
        
    def test_temperature_celsius(self):
        self.ambimate.BoolTemperatureFahrenheit = False
        data = self.ambimate.get_temperature()
        self.assertEqual(data[0], "Temperature")
        self.assertEqual(data[1], (256*self.ambimate.read(0x01)+self.ambimate.read(0x02))/10)
        self.assertEqual(data[2], "°C")

    def test_temperature_fahrenheit(self):
        self.ambimate.BoolTemperatureFahrenheit = True
        data = self.ambimate.get_temperature()
        self.assertEqual(data[0], "Temperature")
        self.assertEqual(data[1], ((((256*self.ambimate.read(0x01)+self.ambimate.read(0x02))/10)*9/5+32)*10+0.5)/10)
        self.assertEqual(data[2], "°F")

    def test_humidity(self):
        data = self.ambimate.get_humidity()
        self.assertEqual(data[0], "Humidity")
        self.assertEqual(data[1], int((256*self.ambimate.read(0x03)+self.ambimate.read(0x04))/100)/10)
        self.assertEqual(data[2], "%")

    def test_get_light(self):
        data = self.ambimate.get_light()
        self.assertEqual(data[0], "Light")
        self.assertEqual(data[1], (256*(self.ambimate.read(0x05) & 0x7F)+self.ambimate.read(0x06)))
        self.assertEqual(data[2], "lx")

    def test_get_audio(self):
        data = self.ambimate.get_audio()
        self.assertEqual(data[0], "Audio")
        self.assertEqual(data[1], (256 * (self.ambimate.read(0x07) & 0x7F) + (self.ambimate.read(0x08) & 0x7F)))
        self.assertEqual(data[2], "dB")

    def test_get_CO2(self):
        data = self.ambimate.get_CO2()
        self.assertEqual(data[0], "CO2")
        self.assertEqual(data[1], 256*self.ambimate.read(0x0B)+self.ambimate.read(0x0C))
        self.assertEqual(data[2], "ppm")

    def test_get_VOC(self):
        data = self.ambimate.get_VOC()
        self.assertEqual(data[0], "VOC")
        self.assertEqual(data[1], 256*self.ambimate.read(0x0D)+self.ambimate.read(0x0E))
        self.assertEqual(data[2], "ppb")

    def test_get_voltage(self):
        data = self.ambimate.get_voltage()
        self.assertEqual(data[0], "Voltage")
        self.assertEqual(data[1], round(((256*(self.ambimate.read(0x09) & 0x7F) + self.ambimate.read(0x0A)) / 1024.0) * (3.3 / 0.330),2))
        self.assertEqual(data[2], "Volts")

    def test_get_event_pirmotion(self):
        data = self.ambimate.get_event_pirmotion()
        self.assertEqual(data[0], "EventMotion")
        self.assertEqual(data[1], bool(self.ambimate.read(0x00) & 0x80) or bool(self.ambimate.read(0x00) & 0x01))
        self.assertEqual(data[2], "PIRMOTION")

    def test_get_event_audio(self):
        data = self.ambimate.get_event_audio()
        self.assertEqual(data[0], "EventAudio")
        self.assertEqual(data[1], bool(self.ambimate.read(0x00) & 0x02))
        self.assertEqual(data[2], "AUDIO")

    def test_get_all(self):
        value_data = {}
        if self.ambimate.BoolEnableTemperature:
            (sensor, value, unit) = self.ambimate.get_temperature()
            value_data[sensor] = (value, unit)
        if self.ambimate.BoolEnableHumidity:
            (sensor, value, unit) = self.ambimate.get_humidity()
            value_data[sensor] = (value, unit)
        if self.ambimate.BoolEnableLight:
            (sensor, value, unit) = self.ambimate.get_light()
            value_data[sensor] = (value, unit)
        if self.ambimate.BoolEnableAudio:
            (sensor, value, unit) = self.ambimate.get_audio()
            value_data[sensor] = (value, unit)
        if self.ambimate.BoolEnableCO2:
            (sensor, value, unit) = self.ambimate.get_CO2()
            value_data[sensor] = (value, unit)
        if self.ambimate.BoolEnableVOC:
            (sensor, value, unit) = self.ambimate.get_VOC()
            value_data[sensor] = (value, unit)
        if self.ambimate.BoolEnableVoltage:
            (sensor, value, unit) = self.ambimate.get_voltage()
            value_data[sensor] = (value, unit)
        if self.ambimate.BoolEnableEventsPIRMOTION:
            (sensor, value, unit) = self.ambimate.get_event_pirmotion()
            value_data[sensor] = (value, unit)
        if self.ambimate.BoolEnableEventsAUDIO:
            (sensor, value, unit) = self.ambimate.get_event_audio()
            value_data[sensor] = (value, unit)
        self.assertEqual(self.ambimate.get_all(), value_data)

if __name__ == "__main__":
    unittest.main()    