import unittest
import os
import sys
import inspect
import random
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))  # Get current dir
parentdir = os.path.dirname(currentdir)  # get parent dir
driverdir = parentdir + "/"
examplesdir=driverdir+"examples"

sys.path.insert(0, driverdir)  # insert path of parent dir in import settings
sys.path.insert(0, examplesdir)  # insert path of parent dir in import settings

from ambimate import Ambimate  # import the driver
from getAllValues import getAllValues_example  # import the driver
from getAllValuesLoop import getAllValuesLoop_example  # import the driver
from getAmbimateDetails import getAmbimateDetails_example  # import the driver
from getSpecificValue import getSpecificValue_example  # import the driver
from getSpecificValueLoop import getSpecificValueLoop_example  # import the driver


class Test_examples(unittest.TestCase):

    def setUp(self):
        self.ambimate = Ambimate()
        self.ambimate.is_unittest = True
    def test_getAllValues(self):
        getAllValues_example()
    def test_getAllValuesLoop(self):
        getAllValuesLoop_example()
    def test_getAmbimateDetails(self):
        getAmbimateDetails_example()
    def test_getSpecificValue(self):
        getSpecificValue_example()
    def test_getSpecificValueLoop(self):
        getSpecificValueLoop_example()
if __name__=="__main__":
    unittest.main()    