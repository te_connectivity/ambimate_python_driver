import os
import sys
import inspect
sys.path.insert(0,os.path.dirname(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))))
from ambimate import Ambimate
def getAmbimateDetails_example():
    ambimate = Ambimate()
    ambimate.BoolTemperatureFahrenheit = False
    FirmwareVersion = ambimate.get_firmware_version()[1]  # Get the firmware version of the code
    FirmwareSubversion = ambimate.get_firmware_subversion()[1]  # Get the sub-firmware version of the code
    OptionalSensors = ambimate.get_optional_sensors()[1]  # All additional sensors

    if not(ambimate.is_raspberry):
        print("You are a computer")
    
    print("AmbiMate Firmware version: %d.%d" % (FirmwareVersion, FirmwareSubversion))
    print("AmbiMate Sensor: 4 Core")
    print("Sensors:")
    print("\t-Temperature")
    print("\t-Humidity")
    print("\t-Light")
    print("\t-Voltage")
    print("\t-Event PIRMOTION")
    print("Additional sensors:")
    if OptionalSensors & 0x01:
        print("\t-CO2")
        print("\t-VOC")
    if OptionalSensors & 0x04:
        print("\t-Audio")
        print("\t-Event Audio")
        
if __name__ == "__main__":
    getAmbimateDetails_example()