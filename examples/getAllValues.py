import os
import sys
import inspect
sys.path.insert(0,os.path.dirname(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))))
from ambimate import Ambimate

def getAllValues_example():
    ambimate = Ambimate()
    ambimate.BoolTemperatureFahrenheit = False
    ambimate.BoolWait = True  # Wait for the measure
    ambimate.SampleTime = 0  # Set sample time to 0 (only for a single data catch!)
    ambimate.setup_sensors()  # Setup all sensors that the Ambimate have
    print(ambimate.get_all())  # Get all data into a dictionary

    # Different data access:
    # print(Ambimate.data)
    # print(Ambimate.Temperature)
    # print(Ambimate.data["Temperature"])
if __name__ == "__main__":
    getAllValues_example()