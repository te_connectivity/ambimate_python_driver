import os
import sys
import inspect
sys.path.insert(0,os.path.dirname(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))))
from ambimate import Ambimate
def getAllValuesLoop_example():
    ambimate = Ambimate()
    ambimate.BoolTemperatureFahrenheit = False
    ambimate.setup_sensors()  # Setup all sensors that the ambimate have
    ambimate.BoolWait=True
    ambimate.SampleTime=0
    while 1:  # Infinite loop
        print(ambimate.get_all())  # Method to get all values
        if not(ambimate.is_raspberry) or ambimate.is_unittest:
            break
if __name__ == "__main__":
    getAllValuesLoop_example()