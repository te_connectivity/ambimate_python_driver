import os
import sys
import inspect
sys.path.insert(0,os.path.dirname(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))))
from ambimate import Ambimate
import time

def getSpecificValueLoop_example():
    ambimate = Ambimate()
    ambimate.BoolTemperatureFahrenheit = False
    # Ambimate settings
    ambimate.BoolTemperatureFahrenheit = False  # For Temperature value in Fahrenheit
    ambimate.BoolPrintSensorResult = True  # Display sensor result for a get...() method
    while 1:
        ambimate.ask_measure()  # Ask the ambimate to start a measure
        ambimate.wait_measure()  # Wait for the end of measure
        print(ambimate.get_temperature())  # get temperature method
        
        # (sensor, value, unit) = ambimate.get_humidity()
        # (sensor, value, unit) = ambimate.get_light()
        # (sensor, value, unit) = ambimate.get_audio()
        # (sensor, value, unit) = ambimate.get_CO2()
        # (sensor, value, unit) = ambimate.get_VOC()
        # (sensor, value, unit) = ambimate.get_voltage()
        # (sensor, value, unit) = ambimate.get_event_audio()
        # (sensor, value, unit) = ambimate.get_event_pirmotion()
        
        
        if not(ambimate.is_raspberry) or ambimate.is_unittest:
            break
        
        

if __name__ == "__main__":
    getSpecificValueLoop_example()