import os
import sys
import inspect
sys.path.insert(0,os.path.dirname(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))))
from ambimate import Ambimate

def getSpecificValue_example():
    ambimate = Ambimate()
    ambimate.BoolTemperatureFahrenheit = False

    # Ask the Ambimate to measure all his sensor
    ambimate.ask_measure()  # Ask the Ambimate to read its sensors

    # Wait for finish measures
    ambimate.wait_measure()  # Wait for the Ambimate to finish measuring its sensors

    # Getting sensor value and display
    print(ambimate.get_temperature())  # Get the temperature
    # (sensor,value,unit)=ambimate.get_humidity()
    # (sensor,value,unit)=ambimate.get_light()
    # (sensor,value,unit)=ambimate.get_audio()
    # (sensor,value,unit)=ambimate.get_CO2()
    # (sensor,value,unit)=ambimate.get_VOC()
    # (sensor,value,unit)=ambimate.get_voltage()

    

    # It is possible to use object.sensorName to get a value after a measure
    # Temperature = ambimate.Temperature
    # Humidity      =   ambimate.Humidity
    # Light         =   ambimate.Light
    # Audio         =   ambimate.Audio
    # CO2           =   ambimate.CO2
    # VOC           =   ambimate.VOC
    # Voltage       =   ambimate.Voltage

if __name__ == "__main__":
    getSpecificValue_example()