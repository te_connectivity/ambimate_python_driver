import sys
import os
import datetime

project = 'Ambimate Python Driver'

author = 'Vincent Bénet'

copyright = 'TE connectivity by '+author
version = 'V 1.0'

master_doc='index'
extensions = []

templates_path = ['_templates']
exclude_patterns = []
html_theme = 'sphinx_rtd_theme'
html_logo = "_static/logo.svg"
html_theme_options = {
    'style_nav_header_background': '#e77617',
    'prev_next_buttons_location': 'both'
}
html_scaled_image_link = False
autosectionlabel_prefix_document = True
html_static_path = ['_static']
html_show_sphinx=True
html_last_updated_fmt=datetime.datetime.now().strftime("%A %d %B %Y - %Hh%M")


rst_prolog = """
.. image:: /_static/logo_orange.svg
    :align: right
    :width: 30%

"""

rst_epilog = """
.. image:: /_static/header.svg
    :width: 100%
"""
