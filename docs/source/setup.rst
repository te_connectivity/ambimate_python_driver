Setup
=================================
Raspbian setup
--------------
In order to use this driver on your raspberry pi, you have to follow theses steps:

- Acces to your raspberry settings: ::
    
    sudo raspi-config

- Enable **I²C**:

``5 - Interfacing Options`` > ``P5 I2C`` > ``<Yes>``

- Enable **GPIO**:

``5 - Interfacing Options`` > ``P8 Remote GPIO`` > ``<Yes>``


Driver setup
--------------

To install the driver, you will need to install git.
First, update and upgrade your packages by running the following commands: ::

    sudo apt-get update
    sudo apt-get upgrade -y

This may take a few minutes, in the mean time, you can `learn more about these commands <https://codeburst.io/a-beginners-guide-to-using-apt-get-commands-in-linux-ubuntu-d5f102a56fc4>`_.

You can then download git by running this command : ::

    sudo apt-get install git

Git is a distributed version control system. It is normally used by developers to track changes and coordinate work on projects. Here you will simply use it so that you can obtain the necessary code for the Ambimate.
This is done by "cloning" the relevant repository with the following command : ::

    git clone https://gitlab.com/te_connectivity/ambimate_python_driver

That's it! You should now have a brand new repository that contains all the necessary files for you to work with your Ambimate module.
You can check that this is the case by running the command ::

    cd ambimate_python_driver
    ls

The ls command lists the files and directory that are in the directory you currently are in.
You should see a new one named ... .

Hardware setup
--------------

.. image:: _static/Branchement.svg
   :width: 45%
   :align: center

You simply need to connect the 5 pins of the Ambimate module to the first five pins of the inside row on the Raspberry Pi, as shown above.
We recommend using a ribbon cable that keeps your Ambimate module away from other electronic components as Joule heating may give you false readings for temperature and other measurements.

.. Warning:: If you invert wires, it will overheat the ambimate module and could destroy it.