Introduction
=================================
Goal
----
This Python library provides the necessary tools to use the `TE Ambimate module <https://www.te.com/usa-en/products/sensors/multi-sensor-modules.html?tab=pgp-story>`_.
The Ambimate is a multi sensor module, with sensors for temperature, humidity, light level, audio level, CO2 concentration, VoC concentration, and motion detection.
This library may be set up for use with any Ambimate sensor and Raspberry Pi. You can find out more about how to configure it for your specific Ambimate sensor by accessing the `Docs`.
This library can also be used for any project where you need the Ambimate module.

Project
-------
It is designed to be used as part of TE Convergence project, which allows to build your own home thermostat for less than $100, with detailed instructions.
You can simply link your Raspberry Pi with actuators (AC, heater, and fan) to control your home environment. 