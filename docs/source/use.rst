Use
=================================


Settings
-----------------
 
 - Sensor settings

+---------------------------+--------------+----------------------------------------------------------+
| **Sensor settings**       | **Type**     | **Description**                                          |
+===========================+==============+==========================================================+
| BoolEnableAudio           | Boolean      | Enable Audio sensor for get_all method                   |
+---------------------------+--------------+----------------------------------------------------------+
| BoolEnableco2             | Boolean      | Enable CO2 sensor for get_all method                     |
+---------------------------+--------------+----------------------------------------------------------+
| BoolEnableEventsPIRMOTION | Boolean      | Enable MotionEvent sensor for get_all method             |
+---------------------------+--------------+----------------------------------------------------------+
| BoolEnableEventsAUDIO     | Boolean      | Enable AudioEVENT event for get_all method               |
+---------------------------+--------------+----------------------------------------------------------+
| BoolEnableHumidity        | Boolean      | Enable Humidity sensor for get_all method                |
+---------------------------+--------------+----------------------------------------------------------+
| BoolEnableLight           | Boolean      | Enable Light sensor for get_all method                   |
+---------------------------+--------------+----------------------------------------------------------+
| BoolEnableTemperature     | Boolean      | Enable Temperature sensor for get_all method             |
+---------------------------+--------------+----------------------------------------------------------+
| BoolEnablevoc             | Boolean      | Enable VOC sensor for get_all method                     |
+---------------------------+--------------+----------------------------------------------------------+
| BoolEnableVoltage         | Boolean      | Enable Voltage sensor for get_all method                 |
+---------------------------+--------------+----------------------------------------------------------+

 - General settings

+---------------------------+--------------+----------------------------------------------------------+
| **General settings**      | **Type**     | **Description**                                          |
+===========================+==============+==========================================================+
| WaitingTime               | Float        | Time that the program waits after ask_measure method     |
+---------------------------+--------------+----------------------------------------------------------+
| SampleTime                | Float        | Time that the program waits at the end of get_all method |
+---------------------------+--------------+----------------------------------------------------------+
| BoolAskMeasureEach        | Boolean      | Runs the ask_measure method between each get ... method  |
+---------------------------+--------------+----------------------------------------------------------+
| BoolTemperatureFahrenheit | Boolean      | get_temperature method's value in Fahrenheit if True     |
+---------------------------+--------------+----------------------------------------------------------+
| BoolWait                  | Boolean      | Triggers wait_measure in ask_measure method if True      |
+---------------------------+--------------+----------------------------------------------------------+
| Address                   | Int          | Bus address of Ambimate (default = 42)                   |
+---------------------------+--------------+----------------------------------------------------------+


Methods
-------
- **setup_sensors**

.. literalinclude:: ../../ambimate.py
   :language: python
   :linenos:
   :pyobject: Ambimate.setup_sensors

- **ask_measure**

.. literalinclude:: ../../ambimate.py
   :language: python
   :linenos:
   :pyobject: Ambimate.ask_measure

- **wait_measure**

.. literalinclude:: ../../ambimate.py
   :language: python
   :linenos:
   :pyobject: Ambimate.wait_measure

- **read**

.. literalinclude:: ../../ambimate.py
   :language: python
   :linenos:
   :pyobject: Ambimate.read

- **write**

.. literalinclude:: ../../ambimate.py
   :language: python
   :linenos:
   :pyobject: Ambimate.write

- **get_firmware_version**

.. literalinclude:: ../../ambimate.py
   :language: python
   :linenos:
   :pyobject: Ambimate.get_firmware_version

- **get_firmware_subversion**

.. literalinclude:: ../../ambimate.py
   :language: python
   :linenos:
   :pyobject: Ambimate.get_firmware_subversion

- **get_optional_sensors**

.. literalinclude:: ../../ambimate.py
   :language: python
   :linenos:
   :pyobject: Ambimate.get_optional_sensors

- **get_temperature**

.. literalinclude:: ../../ambimate.py
   :language: python
   :linenos:
   :pyobject: Ambimate.get_temperature

- **get_humidity**

.. literalinclude:: ../../ambimate.py
   :language: python
   :linenos:
   :pyobject: Ambimate.get_humidity

- **get_light**

.. literalinclude:: ../../ambimate.py
   :language: python
   :linenos:
   :pyobject: Ambimate.get_light

- **get_audio**

.. literalinclude:: ../../ambimate.py
   :language: python
   :linenos:
   :pyobject: Ambimate.get_audio

- **get_co2**

.. literalinclude:: ../../ambimate.py
   :language: python
   :linenos:
   :pyobject: Ambimate.get_CO2

- **get_voc**

.. literalinclude:: ../../ambimate.py
   :language: python
   :linenos:
   :pyobject: Ambimate.get_VOC

- **get_voltage**

.. literalinclude:: ../../ambimate.py
   :language: python
   :linenos:
   :pyobject: Ambimate.get_voltage

- **get_event_pirmotion**

.. literalinclude:: ../../ambimate.py
   :language: python
   :linenos:
   :pyobject: Ambimate.get_event_pirmotion

- **get_event_audio**

.. literalinclude:: ../../ambimate.py
   :language: python
   :linenos:
   :pyobject: Ambimate.get_event_audio

- **get_all**

.. literalinclude:: ../../ambimate.py
   :language: python
   :linenos:
   :pyobject: Ambimate.get_all


Attributes
----------
Every time a get method is used, as well as returning the result, an attribute attached to the Ambimate object is updated. 
After using the method get_temperature, you can get the temperature using 2 differents ways.

::

  ambi=Ambimate()
  ambi.ask_measure()
  ambi.wait_measure()
  temperature=ambi.get_temperature()
  temperature=ambi.Temperature

For the get_all method, it return a dictionnary and attach it to the Ambimate
After using the get_all method, you can get the temperature by 3 differents way:

::

  ambi=Ambimate()
  ambi.BoolWait=True
  data=ambi.get_all()
  temperature=data["Temperature"]
  temperature=ambi.data["Temperature"]
  temperature=ambi.Temperature

Bellow all attributes:

+------------------------+--------------+----------------------------------------------------------+
| **Attributes**         | **Type**     | **Description**                                          |
+========================+==============+==========================================================+
| FirmwareVersion        | Int          | Get Firmware version after get_firmware_version method   |
+------------------------+--------------+----------------------------------------------------------+
| FirmwareSubversion     | Int          | Get sub version after get_firmware_subversion method     |
+------------------------+--------------+----------------------------------------------------------+
| Temperature            | Float        | Get temperature after get_temperature method             |
+------------------------+--------------+----------------------------------------------------------+
| Humidity               | Float        | Get humidity after get_temperature method                |
+------------------------+--------------+----------------------------------------------------------+
| Light                  | Float        | Get light after get_temperature method                   |
+------------------------+--------------+----------------------------------------------------------+
| Audio                  | Float        | Get audio after get_temperature method                   |
+------------------------+--------------+----------------------------------------------------------+
| co2                    | Float        | Get co2 after get_temperature method                     |
+------------------------+--------------+----------------------------------------------------------+
| voc                    | Float        | Get voc after get_temperature method                     |
+------------------------+--------------+----------------------------------------------------------+
| Voltage                | Float        | Get voltage after get_temperature method                 |
+------------------------+--------------+----------------------------------------------------------+
| Events_pirMotion       | Boolean      | Get pir event after get_event_pirmotion method           |
+------------------------+--------------+----------------------------------------------------------+
| Events_audio           | Boolean      | Get pir event after get_event_pirmotion method           |
+------------------------+--------------+----------------------------------------------------------+
| data                   | Dictionary   | Dictionary with all data after get_all method            |
+------------------------+--------------+----------------------------------------------------------+
| is_raspberry           | Boolean      | Tell the unit test if the machine is a raspberry         |
+------------------------+--------------+----------------------------------------------------------+




Examples
-----------------
- **getSpecificValueLoop.py**

This example measure a specific value every X second.

.. literalinclude:: ../../examples/getSpecificValueLoop.py
  :language: python
  :linenos:

- **getSpecificValue.py**

This example measure specifics values once.

.. literalinclude:: ../../examples/getSpecificValue.py
  :language: python
  :linenos:

- **getAmbimateDetails.py**

This example provide informations on your ambimate.

.. literalinclude:: ../../examples/getAmbimateDetails.py
  :language: python
  :linenos:

- **getAllValuesLoop.py**

This example measure all value for a period of a second.

.. literalinclude:: ../../examples/getAllValuesLoop.py
  :language: python
  :linenos:

- **getAllValues.py**

This example measure all value once.

.. literalinclude:: ../../examples/getAllValues.py
  :language: python
  :linenos:


Ambimate Object
---------------
.. literalinclude:: ../../ambimate.py
   :language: python
   :linenos:
   :pyobject: Ambimate