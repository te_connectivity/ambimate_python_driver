Ambimate technical documentation
=================================

This page provides an overview of the Ambimate module technical details.

Dimensions
----------
.. image:: _static/Measures.svg
   :width: 45%
   :align: center



Datasheet
---------------
You can find the `Ambimate datasheet <https://www.te.com/commerce/DocumentDelivery/DDEController?Action=showdoc&DocId=Specification+Or+Standard%7F114-133092%7FE%7Fpdf%7FEnglish%7FENG_SS_114-133092_E.pdf%7F2314277-1>`_,
the `product flyer <https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1-1773935-7_AmbiMate_MS4_Series&DocType=DS&DocLang=EN>`_,
and access our `Ambimate products <https://www.te.com/usa-en/product-CAT-AM16-M8797.html#guest-click>`_.


Address memory
---------------


This table gives the register address corresponding to the data extracted from the Ambimate.

+----------------------------------+------------------------+----------------------+
| **Type of data**                 | **Data**               | **Register Address** |
+==================================+========================+======================+
| Sensor data                      | Status High Byte       | 0x00                 |
|                                  +------------------------+----------------------+
|                                  | Temperature High Byte  | 0x01                 |                     
|                                  +------------------------+----------------------+
|                                  | Temperature Low Byte   | 0x02                 |                    
|                                  +------------------------+----------------------+
|                                  | Humidity High Byte     | 0x03                 |                  
|                                  +------------------------+----------------------+
|                                  | Humidity Low Byte      | 0x04                 |                 
|                                  +------------------------+----------------------+
|                                  | Light High Byte        | 0x05                 |               
|                                  +------------------------+----------------------+
|                                  | Light Low Byte         | 0x06                 |              
|                                  +------------------------+----------------------+
|                                  | Audio High Byte        | 0x07                 |               
|                                  +------------------------+----------------------+
|                                  | Audio Low Byte         | 0x08                 |              
|                                  +------------------------+----------------------+
|                                  | Battery Volts High Byte| 0x09                 |                       
|                                  +------------------------+----------------------+
|                                  | Battery Volts Low Byte | 0x0A                 |                      
|                                  +------------------------+----------------------+
|                                  | eCO2 High Byte         | 0x0B                 |              
|                                  +------------------------+----------------------+
|                                  | eCO2 Low Byte          | 0x0C                 |             
|                                  +------------------------+----------------------+
|                                  | VOC High Byte          | 0x0D                 |             
|                                  +------------------------+----------------------+
|                                  | VOC Low Byte           | 0x0E                 |            
+----------------------------------+------------------------+----------------------+
| 8-bit High Byte Sensor data      | Status High Byte       | 0x40                 |                
|                                  +------------------------+----------------------+
|                                  | Temperature High Byte  | 0x41                 |                     
|                                  +------------------------+----------------------+
|                                  | Humidity High Byte     | 0x42                 |                  
|                                  +------------------------+----------------------+
|                                  | Light High Byte        | 0x43                 |               
|                                  +------------------------+----------------------+
|                                  | Audio High Byte        | 0x44                 |               
|                                  +------------------------+----------------------+
|                                  | Battery Volts High Byte| 0x45                 |                       
|                                  +------------------------+----------------------+
|                                  | eCO2 High Byte         | 0x46                 |              
|                                  +------------------------+----------------------+
|                                  | VOC High Byte          | 0x47                 |             
+----------------------------------+------------------------+----------------------+
| Ambimate data                    | Firmware version       | 0x80                 |                
|                                  +------------------------+----------------------+
|                                  | Firmware sub-version   | 0x81                 |                    
|                                  +------------------------+----------------------+
|                                  | Optional Sensors       | 0x82                 |                
+----------------------------------+------------------------+----------------------+
| Writeable register               | Scan Start Byte        | 0xC0                 |               
+----------------------------------+------------------------+----------------------+

 - Event Byte

+----------------+----------+
| **Event Byte** | **Bit**  |        
+================+==========+
| Bit 0          | PIR      |
+----------------+----------+
| Bit 1          | Audio    |
+----------------+----------+
| Bit 2          | Reserved |
+----------------+----------+
| Bit 3          | Reserved |
+----------------+----------+
| Bit 4          | Reserved |
+----------------+----------+
| Bit 5          | Reserved |
+----------------+----------+
| Bit 6          | Reserved |
+----------------+----------+
| Bit 7          | PIR EVENT|
+----------------+----------+



Sensors
-------------                        

This table gives the formula used in the driver to calculate the value of the various parameters.

+--------------+----------+-----------+--------------------------------------------+
| **Type**     |**Sensor**| **Unit**  | **Formula**                                |
+==============+==========+===========+============================================+
|              |          | °C        | .. math:: (256\times b1+b2)/10             |
| Temperature  |          +-----------+--------------------------------------------+
|              |          | °F        | .. math:: (256\times b1+b2)\times9/50+32   |
+--------------+----------+-----------+--------------------------------------------+
| Humidity     |          | %         | .. math:: (256\times b3+b4)/10             |
+--------------+----------+-----------+--------------------------------------------+
| Light        |          | Lux       | .. math:: 256\times(b5\cdot127)+b6         |
+--------------+----------+-----------+--------------------------------------------+
| Audio        |          | dB        | .. math:: 256\times(b7\cdot127)+b8\cdot127 |
+--------------+----------+-----------+--------------------------------------------+
| CO2          |          | ppm       | .. math:: 256\times b11+b12                |
+--------------+----------+-----------+--------------------------------------------+
| VOC          |          | ppb       | .. math:: 256\times b13+b14                |
+--------------+----------+-----------+--------------------------------------------+
| Voltage      |          | Volt      | .. math:: 256\times(b9\cdot127)+b10        |
+--------------+----------+-----------+--------------------------------------------+
| MotionEvent  |          | Boolean   | .. math:: (b0\cdot128) || (b0\cdot1)       |
+--------------+----------+-----------+--------------------------------------------+
| AudioEvent   |          | Boolean   | .. math:: b0\cdot2                         |
+--------------+----------+-----------+--------------------------------------------+


Options
----------------
Four core sensing capabilities:
 - Motion (PIR)
 - Light
 - Temperature
 - Humidity
 
Optional sensors:
 - VOC
 - CO2
 - Audio

Physical attachment methods:
 - Castellation
 - Vertical Header
 - Plated Thru Hole

Unit Tests
----------
All methods are tested with Unittest. These tests may be run to ensure the driver works well after installation and updates.
Tests are made with python 3 into a Runner. The tests may be run by accessing the tests directory.

.. literalinclude:: ../../tests/test_driver.py
   :language: python
   :linenos:
