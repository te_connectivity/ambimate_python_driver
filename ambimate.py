''' 
   Copyright 2020 TE Connectivity
   Distributed under MIT license.
   See file LICENSE for detail or copy at https://opensource.org/licenses/MIT
'''

import time
import threading
import sys


class Ambimate:
    def __init__(self):
        self.FirmwareVersion = None  # Get FirmwareVersion if the function associated is executed
        self.FirmwareSubversion = None  # Get FirmwareSubversion if the function associated is executed
        self.Temperature = None  # Get Temperature if the function associated is executed
        self.Humidity = None  # Get Humidity if the function associated is executed
        self.Light = None  # Get Light if the function associated is executed
        self.Audio = None  # Get Audio if the function associated is executed
        self.CO2 = None  # Get CO2 if the function associated is executed
        self.VOC = None  # Get VOC if the function associated is executed
        self.Voltage = None  # Get Voltage if the function associated is executed
        self.Events_pirMotion = None  # Get Events_pirMotion if the function associated is executed
        self.Events_audio = None  # Get Events_audio if the function associated is executed
        self.data = {}  # Dictionary that stores values for get_all()
        self.Address = 0x2A  # Address of an ambimate module
        self.BoolAskMeasureEach = True  # Ask measure for each data
        self.BoolEnableAudio = True  # Enable Audio in getAll function
        self.BoolEnableCO2 = True  # Enable CO2 in getAll function
        self.BoolEnableEventsPIRMOTION = True  # Enable Events MOTION in getAll function
        self.BoolEnableEventsAUDIO = True  # Enable Events AUDIO in getAll function
        self.BoolEnableHumidity = True  # Enable Humidity in getAll function
        self.BoolEnableLight = True  # Enable Light in getAll function
        self.BoolEnableTemperature = True  # Enable Temperature in getAll function
        self.BoolEnableVOC = True  # Enable VOC in getAll function
        self.BoolEnableVoltage = True  # Enable Voltage in getAll function
        self.BoolTemperatureFahrenheit = True  # Temperature in Fahrenheit if True, else in Celsius
        self.BoolWait = True  # Enable waiting after ing for a measure
        self.Events_pirmotion = True
        self.WaitingTime = 0.400  # Waiting time after a measure
        self.SampleTime = 0.700  # Waiting time after a measure
        self.seed = time.time()
        self.is_unittest = False
        try:
            import smbus
            import RPi.GPIO
            self.bus = smbus.SMBus(0x01)  # Bus connection
            self.is_raspberry = True
        except:
            self.is_raspberry = False
        self.opt_sensors = self.get_optional_sensors()[1]  # Ask the ambimate his sensors
        self.hasCO2 = bool(self.opt_sensors & 0x01)  # Mask of ambimate answer for CO2 sensor
        self.hasAUDIO = bool(self.opt_sensors & 0x04)  # Mask of ambimate answer for Audio sensor
        self.disable_all_sensor()
        self.setup_sensors()
        self.ask_measure()
    
    def sleep(self,temps):
        time.sleep(max(0, temps) * self.is_raspberry * (not(self.is_unittest)))
    
    def disable_all_sensor(self):
        self.BoolEnableAudio = False
        self.BoolEnableCO2 = False
        self.BoolEnableEventsPIRMOTION = False
        self.BoolEnableEventsAUDIO = False
        self.BoolEnableHumidity = False
        self.BoolEnableLight = False
        self.BoolEnableTemperature = False
        self.BoolEnableVOC = False
        self.BoolEnableVoltage = False
    
    def setup_sensors(self):  # Automatically turn on every sensor of the ambimate
        '''
        This method is used to enable all the core sensors of the Ambimate module.
        It also enables any additional sensors that may be present.
        '''
        self.BoolEnableTemperature = True  # Turn On temperature
        self.BoolEnableHumidity = True  # Turn On humidity
        self.BoolEnableLight = True  # Turn On light
        self.BoolEnableEventsPIRMOTION = True  # Turn On motionDetection
        self.BoolEnableVoltage = True  # Turn On voltage
        self.BoolEnableCO2 = self.hasCO2
        self.BoolEnableVOC = self.hasCO2
        self.BoolEnableAudio = self.hasAUDIO
        self.BoolEnableEventsAUDIO = self.hasAUDIO
        if not(self.is_raspberry):
            self.BoolEnableCO2 = True
            self.BoolEnableVOC = True
            self.BoolEnableAudio = True
            self.BoolEnableEventsAUDIO = True

    def ask_measure(self):  # Ask the ambimate to read all sensors and store results in its memory
        '''
        This method is used to read the value of all the sensors present through the bus.
        This information is then written to the Ambimate's memory.
        :return:
        '''
        self.write(0xC0, 0xa5)  # Reset all datas stored in the ambimate
        self.write(0xC0, 0x7F if self.hasCO2 else 0x3F)  # Ask the ambimate to read all sensors (CO2 included) by bus
        if self.BoolWait:
            self.wait_measure()

    def wait_measure(self):  # Method that just waits after a measure
        '''
        This method waits until a measure is completed.
        :return:
        '''
        self.WaitingTime = max(0.1,self.WaitingTime)
        self.sleep(self.WaitingTime)

    def read(self, address_data):  # Method to read a data into the ambimate memory using bus
        '''
        This method reads the data associated to a specific address in the Ambimate's memory.
        The data was stored there by the ask_measure method.
        :param address_data:
        :return:
        '''
        if address_data > 0xFF or address_data < 0x00:
            raise ValueError("address_data " + str(address_data) + " is not valid")
        if not(str(type(address_data)) in ["<type 'int'>","<class 'int'>"]):
            raise TypeError("address_data " + str(address_data) + " is not int but " + str(type(address_data)))
        value = 0
        try:
            value = self.bus.read_byte_data(self.Address, address_data)  # Get data of the ambimate
        except IOError:  # IO read error (1 time on 200 read)
            print("Ambimate IO read error")
            if(self.is_unittest == False):
                return IOError
        except AttributeError:
            value = 0xFF
        if not(str(type(value)) in ["<type 'int'>","<class 'int'>"]):
            raise TypeError("value " + str(value) + " is not int but " + str(type(data)))
        return value  # Return the bus value

    def write(self, address_data, data):  # Method to write on bus address
        '''
        This method is used to write data to a specific address in the Ambimate's memory.
        It is only used to ask the Ambimate to measure data.
        :param address_data:
        :param data:
        :return:
        '''
        if address_data > 0xFF or address_data < 0x00:
            raise ValueError("address_data " + str(address_data) + " is not valid")
        if data > 0xFF or data < 0x00:
            raise ValueError("data " + str(address_data) + " is not valid")
        if not(str(type(address_data)) in ["<type 'int'>","<class 'int'>"]):
            raise TypeError("address_data " + str(address_data) + " is not int but "+str(type(address_data)))
        if not(str(type(data)) in ["<type 'int'>","<class 'int'>"]):
            raise TypeError("data " + str(data) + " is not int but " + str(type(data)))
        try:
            self.bus.write_byte_data(self.Address, address_data, data)  # Write on the bus
        except IOError:  # IO read error (1 time on 200 read)
            print("Ambimate IO write error")
        except AttributeError:
            pass
                

    def get_firmware_version(self):  # Method to get the ambimate firmware version
        '''
        This method is used to get the firmware version of the user's Ambimate module.
        :return:
        '''
        value = self.read(0x80)  # Read the measure of the ambimate
        self.FirmwareVersion = value  # Attach the value to the object
        return "Firmware", value, "Version"  # Return full measure details

    def get_firmware_subversion(self):
        '''
        This method is used to get the firmware subversion of the user's Ambimate module.
        :return:
        '''
        value = self.read(0x81)  # Read the measure of the ambimate
        self.FirmwareSubversion = value  # Attach the value to the object
        return "SubFirmware", value, "Subversion"  # Return full measure details

    def get_optional_sensors(self):
        '''
        This method gives which extra sensors are present besides the core sensors in the user's Ambimate module.
        :return:
        '''
        value = self.read(0x82)  # Read the measure of the ambimate
        return "Sensors", value, "Option"  # Return full measure details

    def get_temperature(self):
        '''
        This method is used to obtain the temperature measurement.
        The user may choose whether the unit of measurement (C or F) using the BoolTemperatureFahrenheit attribute.
        :return:
        '''
        if self.BoolTemperatureFahrenheit:
            unit = "°F"
        else:
            unit = "°C"
        if self.BoolAskMeasureEach:
            self.ask_measure()
        value = (256 * self.read(0x01) + self.read(0x02)) / 10  # Read the measure of the ambimate
        if self.BoolTemperatureFahrenheit:
            value = ((value * 9 / 5 + 32) * 10 + 0.5) / 10
        self.Temperature = value  # Attach the value to the object
        return "Temperature", value, unit  # Return full measure details

    def get_humidity(self):
        '''
        This method is used to get the relative humidity.
        The value given is in % of saturated vapour pressure.
        :return:
        '''
        if self.BoolAskMeasureEach:
            self.ask_measure()
        value = int((256 * self.read(0x03) + self.read(0x04)) / 100) / 10  # Read the measure of the ambimate
        self.Humidity = value  # Attach the value to the object
        return "Humidity", value, "%"  # Return full measure details

    def get_light(self):
        '''
        This method is used to get the illuminance reading from the light sensor.
        The value is given in lux.
        :return:
        '''
        if self.BoolAskMeasureEach:
            self.ask_measure()
        value = (256 * (self.read(0x05) & 0x7F) + self.read(0x06))  # Read the measure of the ambimate
        self.Light = value  # Attach the value to the object
        return "Light", value, "lx"  # Return full measure details

    def get_audio(self):
        '''
        This method is used to read the sound intensity from the audio sensor.
        The value is given in decibels.
        :return:
        '''
        if self.BoolAskMeasureEach:
            self.ask_measure()
        value = (256 * (self.read(0x07) & 0x7F) + (self.read(0x08) & 0x7F))  # Read the measure of the ambimate
        self.Audio = value  # Attach the value to the object
        return "Audio", value, "dB"  # Return full measure details

    def get_CO2(self):
        '''
        This method is used to read the value of CO2 level from the CO2 sensor (if it is present).
        The value is given in parts per million (ppm).
        :return:
        '''
        if self.BoolAskMeasureEach:
            self.ask_measure()
        value = 256 * self.read(0x0B) + self.read(0x0C)  # Read the measure of the ambimate
        self.CO2 = value  # Attach the value to the object
        return "CO2", value, "ppm"  # Return full measure details

    def get_VOC(self):
        '''
        This method is used to read the value of Volatile organic Compounds (VOC) level from the VOC sensor.
        The value is given in parts per billion (ppb).
        :return:
        '''
        if self.BoolAskMeasureEach:
            self.ask_measure()
        value = 256 * self.read(0x0D) + self.read(0x0E) # Read the measure of the ambimate
        self.VOC = value  # Attach the value to the object
        return "VOC", value, "ppb"  # Return full measure details

    def get_voltage(self):
        '''
        This method is used to give the voltage between the 3v3 pin and ground in Volts.
        This is only useful for users seeking to power their Ambimate module with a battery.
        :return:
        '''
        if self.BoolAskMeasureEach:
            self.ask_measure()
        value = round(((256 * (self.read(0x09) & 0x7F) + self.read(0x0A)) / 1024.0) * (3.3 / 0.330), 2)  # Read the measure
        self.Voltage = value  # Attach the value to the object
        return "Voltage", value, "Volts"  # Return full measure details

    def get_event_pirmotion(self):
        '''
        This method is used to detect whether a motion event has been detected during a measure.
        Each time this method is executed, it resets the relevant bit to 0, regardless of its previous value.
        This implies that we need to run the ask_measure method before running this method.
        :return:
        '''
        if self.BoolAskMeasureEach:
            self.ask_measure()
        event = self.read(0x00)
        value = bool(event & 0x80) or bool(event & 0x01)  # Read the measure of the ambimate
        self.Events_pirmotion = value  # Attach the value to the object
        return "EventMotion", value, "PIRMOTION"  # Return full measure details

    def get_event_audio(self):
        '''
        This method is used to detect whether an audio event has been detected during a measure.
        Each time this method is executed, it resets the relevant bit to 0, regardless of its previous value.
        This implies that we need to run the ask_measure method before running this method.
        Note that this is different to the get_audio method, which returns intensity of sound as opposed to this
        one which simply tells the user whether a sound event has occured.
        :return:
        '''
        if self.BoolAskMeasureEach:
            self.ask_measure()
        value = bool(self.read(0x00) & 0x02)  # Read the measure of the ambimate
        self.Events_audio = value  # Attach the value to the object
        return "EventAudio", value, "AUDIO"  # Return full measure details

    def get_all(self):
        '''
        This method gives a dictionary with all the available data using all the above methods.
        :return:
        '''
        time_loop = time.time()  # Start time
        data = {}  # Creation of an empty dictionary
        bool_ask_measure_each = self.BoolAskMeasureEach  # Store setting
        self.ask_measure()  # To empty the buffer
        self.BoolAskMeasureEach = False  # Disable waiting for each measure
        if self.BoolEnableTemperature:
            (sensor, value, unit) = self.get_temperature()  # Get temperature
            data[sensor] = (value, unit)  # Store value in dictionary
        if self.BoolEnableHumidity:
            (sensor, value, unit) = self.get_humidity()  # Get humidity
            data[sensor] = (value, unit)  # Store value in dictionary
        if self.BoolEnableLight:
            (sensor, value, unit) = self.get_light()  # Get ambient light
            data[sensor] = (value, unit)  # Store value in dictionary
        if self.BoolEnableAudio:
            (sensor, value, unit) = self.get_audio()  # Get audio level
            data[sensor] = (value, unit)  # Store value in dictionary
        if self.BoolEnableCO2:
            (sensor, value, unit) = self.get_CO2()  # Get CO2 (refresh every 60 sec)
            data[sensor] = (value, unit)  # Store value in dictionary
        if self.BoolEnableVOC:
            (sensor, value, unit) = self.get_VOC()  # Get VOC (refresh every 60 sec)
            data[sensor] = (value, unit)  # Store value in dictionary
        if self.BoolEnableVoltage:
            (sensor, value, unit) = self.get_voltage()  # Get voltage
            data[sensor] = (value, unit)  # Store value in dictionary
        if self.BoolEnableEventsPIRMOTION:
            (sensor, value, unit) = self.get_event_pirmotion()  # Get pirmotion event state
            data[sensor] = (value, unit)  # Store value in dictionary
        if self.BoolEnableEventsAUDIO:
            (sensor, value, unit) = self.get_event_audio()  # Get audio event state
            data[sensor] = (value, unit)  # Store value in dictionary
        self.BoolAskMeasureEach = bool_ask_measure_each  # Restore old setting
        self.data = data  # Attach dictionary to object
        self.sleep(self.SampleTime-(time.time() - time_loop))
        return data  # Return dictionary